package org.hillel.riabchuk.reflection;

import org.hillel.riabchuk.annotations.People;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Reflection {

    @Test
    public void reflection() {
        People people = new People();
        Class myClass = people.getClass();

        Class mySecondClass = People.class;

        System.out.println(Arrays.toString(myClass.getFields()));
        System.out.println(myClass.getModifiers());
        System.out.println(Arrays.toString(people.getClass().getMethods()));

        Package pack = myClass.getPackage();
        System.out.println(pack.getName());

        Class[] interfaces = myClass.getInterfaces();
        Annotation[] annotationsClass = myClass.getAnnotations();

        for (int i = 0; i<interfaces.length; i++) {
            System.out.print(i == 0 ? "Implements " : ", ");
            System.out.print(interfaces[i].getSimpleName());
        }

        Arrays.asList(annotationsClass).forEach(anotation -> System.out.println("\n" + anotation.annotationType().getSimpleName()));

        Field[] fields = myClass.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("\t" + field.getModifiers() + " " + field.getType() + " " + field.getName() + ";");
        }

        Method[] methods = myClass.getDeclaredMethods();
        for (Method method : methods) {

            Annotation[] annotations = method.getAnnotations();
            System.out.print("\t");
            for (Annotation annotation : annotations) {
                System.out.print("@" + annotation.annotationType().getSimpleName() + " ");
            }
            System.out.println();

            System.out.println("\t" + method.getModifiers() + " " + method.getReturnType() + " " + method.getName());
        }
    }
}
