package org.hillel.riabchuk.Inner;

import org.hillel.riabchuk.inner.Outer;
import org.junit.Test;

public class OuterTest {

    public static int x, x1;
    public int y;

    {
        y = 3;
        x1 = 6;
    }

    static {
        x = 11;
    }

    public void print() {
        System.out.println(x + x1 + y);
    }

    public static void print2() {
        System.out.println(x + x1);
    }

    public void test() {
        OuterTest outerTest = new OuterTest();
        outerTest.print();
        OuterTest.print2();
    }

    @Test
    public void testOuter() {

        Outer.Nested nested = new Outer.Nested();
        nested.printNestedText();

        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();
        inner.printText();
        inner.printStaticText();
        outer.doIt();

        Outer outer1 = new Outer() {
            public int x = 10;

            @Override
            public void doIt() {
                System.out.println("Overrided method doIt()");
            }
        };

        outer1.doIt();

        System.out.println();
        System.out.println("local");
        outer.local();
    }
}
