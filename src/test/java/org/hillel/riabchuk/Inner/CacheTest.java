package org.hillel.riabchuk.Inner;

import org.hillel.riabchuk.inner.Cache;
import org.hillel.riabchuk.inner.Outer;
import org.junit.Test;

public class CacheTest {

    @Test
    public void testCache() {


        String key = "Password";
        String key2 = "Name";

        Integer pass = 1567888;
        String name = "Ivan";

        Cache cache = new Cache();
        cache.store(key, pass);
        cache.store(key2, name);

        System.out.println(cache.get("Password"));
        System.out.println(cache.getData("Password"));
        System.out.println(cache.get("Name"));
    }


}
