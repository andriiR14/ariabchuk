package org.hillel.riabchuk.inheritance;

import org.hillel.riabchuk.inheritance.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CylinderTest {

    @Test
    public void cylinder() {

        List<Figure> list = new ArrayList<>();

        Cylinder c1 = new Cylinder();
        System.out.println("Cylinder:"
                + " radius: " + c1.getRadius()
                + " height: " + c1.getHeight()
                + " base area: " + c1.getArea()
                + " volume: " + c1.getVolume());

        list.add(c1);

        c1 = new Cylinder(10);
        System.out.println("Cylinder:"
                + " radius: " + c1.getRadius()
                + " height: " + c1.getHeight()
                + " base area: " + c1.getArea()
                + " volume: " + c1.getVolume());

        list.add(c1);

        c1 = new Cylinder(2, 10);
        System.out.println("Cylinder:"
                + " radius: " + c1.getRadius()
                + " height: " + c1.getHeight()
                + " base area: " + c1.getArea()
                + " volume: " + c1.getVolume());

        list.add(c1);

        list.forEach(figure -> System.out.println(figure.toString()));
    }
}
