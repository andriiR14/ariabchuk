package org.hillel.riabchuk.inheritance;

import org.junit.Test;

public class TriangleTest {

    @Test
    public void triangle() {

        Poligon t = new Triangle();
        System.out.println(t.toString());

        System.out.println();

        t = new Triangle(1.3, 2.0, 1.3);
        System.out.println(t.toString());

    }
}
