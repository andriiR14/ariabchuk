package org.hillel.riabchuk.string;

import org.junit.Test;

import java.util.List;

import static org.hillel.riabchuk.string.StringTokenizerUtil.*;
import static org.hillel.riabchuk.string.ReadFile.*;

public class StringTokenizerUtilTest {

    @Test
    public void testSplitingBySpace() {
        String text = "Once upon a time ...";

        splitBySpace(text);
    }

    @Test
    public void testSplitingByComma() {
        String text = "Once upon a time, there was gray wolf, who liked red hats";

        splitByComma(text);
    }

    @Test
    public void readCSVTest() {
        String path = "/home/hillel5/IdeaProjects/arHillelElem/src/main/resources/file.csv";
        List<Employee> employeeList = readCSV(path);

        System.out.println();
        employeeList.forEach(x -> System.out.println(x.toString()));
    }
}
