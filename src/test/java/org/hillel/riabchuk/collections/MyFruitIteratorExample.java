package org.hillel.riabchuk.collections;


import org.junit.Test;

public class MyFruitIteratorExample {

    @Test
    public void test() {

        MyOwnArrayList<String> fruitList = new MyOwnArrayList();
        fruitList.add("Mango");
        fruitList.add("Strawberry");
        fruitList.add("Papaya");
        fruitList.add("Watermelon");

        for(int i = 0; i < fruitList.size(); i++) {
            System.out.println(fruitList.get(i));
        }

        System.out.println("---Calling my iterator on my ArrayList-----");

        FruitIterator it = fruitList.iterator();
        while (it.hasNext()) {
            String s = (String)it.next();
            System.out.println("Value: " + s);
        }

        System.out.println("--Fruit List size: " + fruitList.size());
        fruitList.remove(1);
        System.out.println("---After removal, Fruit List size: " + fruitList.size());

        for(int i = 0; i < fruitList.size(); i++) {
            System.out.println(fruitList.get(i));
        }

        MyOwnArrayList<Integer> myOwnArrayList = new MyOwnArrayList<>();
        myOwnArrayList.add(1);

        for(int i = 0; i < myOwnArrayList.size(); i++) {
            System.out.println(myOwnArrayList.get(i));
        }

    }
}
