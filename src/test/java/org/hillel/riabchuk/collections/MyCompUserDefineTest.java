package org.hillel.riabchuk.collections;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class MyCompUserDefineTest {

    @Test
    public void test() {

        Set<Integer> integers = new HashSet<>();
        integers.add(2);
        integers.add(2);
        integers.add(3);

        for (Integer integer : integers) {
            System.out.println(integer);
        }

        System.out.println();

        Set<Integer> integerSet = new TreeSet<>();
        integerSet.add(2);
        integerSet.add(6);
        integerSet.add(1);
        integerSet.add(8);

        integerSet.forEach(x-> System.out.println(x));

        System.out.println();

        Set<Empl> empls = new TreeSet<>(new MyNameComp());
        empls.add(new Empl("Andrii", 5000));
        empls.add(new Empl("Vova", 1000));
        empls.add(new Empl("Denis", 2000));
        empls.add(new Empl("Olha", 3000));

        for (Empl e : empls) {
            System.out.println(e);
        }

    }
}
