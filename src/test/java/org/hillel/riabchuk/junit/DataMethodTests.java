package org.hillel.riabchuk.junit;

import org.hillel.riabchuk.utils.SoftAssert;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;

public class DataMethodTests {

    @Test
    public void testFindMax() throws FileNotFoundException {

        int test = DataMethods.findMax(new int[] {1,2,3,4,5,6});
        //assert test == 7;

        //SoftAssert.assertEquals(5, test);
        SoftAssert.assertEquals(6, test);
        SoftAssert.assertEquals(3, test);
        SoftAssert.assertEquals(5, test);
        SoftAssert.assertEquals(7, test);
        SoftAssert.assertEquals(11, test);

    }

    @Test
    public void testMultiplyByZero() {
        Assert.assertEquals("10 x 0 must be 0", 1, DataMethods.multiply(10, 0));
        Assert.assertEquals("0 x 10 must be 0", 0, DataMethods.multiply(0, 10));
        Assert.assertEquals("0 x 0 must be 0", 0, DataMethods.multiply(0, 0));
    }
}
