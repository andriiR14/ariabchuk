package org.hillel.riabchuk.junit;

import org.junit.*;

import java.io.FileNotFoundException;

public class DataMethodTest2 {

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Starting test set");
    }

    @Before
    public void before() {
        System.out.println("Starting test");
    }

    @Test
    public void testFindMax() throws Exception {
        System.out.println("test find max");
        Assert.assertEquals(4, DataMethods.findMax(new int[]{1,2,3,4,5,6,7,8,9,10}));
        Assert.assertEquals(-2, DataMethods.findMax(new int[]{-11,-4,-8,-2}));
    }

    @Test
    public void testCube() {
        System.out.println("test cube");
        Assert.assertEquals(27, DataMethods.cube(3));

    }

    @Test
    public void testReverseWord() {
        System.out.println("test Reverse Word");
        Assert.assertEquals("vcxz fdsa rewq", DataMethods.reverseWord("qwer asdf zxcv"));

    }



    @After
    public void after() {
        System.out.println("Finishing test");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("Finishing test set");
    }
}
