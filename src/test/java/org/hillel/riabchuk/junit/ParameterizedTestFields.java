package org.hillel.riabchuk.junit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

@RunWith(Parameterized.class)
public class ParameterizedTestFields {

    @Parameterized.Parameter(0)
    public int m1;
    @Parameterized.Parameter(1)
    public int m2;
    @Parameterized.Parameter(2)
    public int result;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] {{4,2,8}, {5,3,15}, {121,4,484}};
        return Arrays.asList(data);
    }

    @Test
    public void testMultiplyException() {
        Assert.assertEquals("Result", result, DataMethods.multiply(m1, m2));
    }
}
