package org.hillel.riabchuk.nio;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestReaderFile {

    final static String POM = "/home/javaelizarenko/IdeaProjects/javaelem/ariabchuk/pom.xml";

    @Test
    public void testReader() {

        try (Stream<String> stream = Files.lines(Paths.get(POM))) {

            stream.forEach(System.out::println);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testSecondReader() throws IOException {

        List<String> list = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(POM))) {

            list = stream.filter(line -> line.startsWith("<"))
                    .map(String::toUpperCase)
                    .collect(Collectors.toList());

        } catch (IOException ex) {
            ex.printStackTrace();
            throw ex;
        }

        list.forEach(System.out::println);

    }
}
