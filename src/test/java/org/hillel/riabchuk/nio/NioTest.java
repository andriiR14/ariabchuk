package org.hillel.riabchuk.nio;

import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.HashSet;
import java.util.Set;

public class NioTest {

    final static String PROJECT_PATH = "/home/javaelizarenko/IdeaProjects/javaelem/ariabchuk/temp/";
    final static String FIRST_FILE = PROJECT_PATH + "new\rTest.txt";

    @Test
    public void copy() {
        Path pathSource = Paths.get(PROJECT_PATH, "paths.txt");

        File file = new File(FIRST_FILE);

        try(FileOutputStream destination = new FileOutputStream(file);
            FileInputStream source = new FileInputStream(file)) {

            long noOfBytes = Files.copy(pathSource, destination);
            System.out.println(noOfBytes);

            Path destPath1 = Paths.get(PROJECT_PATH, "newTest2.txt");
            noOfBytes = Files.copy(source, destPath1, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(noOfBytes);

            Path destPath2 = Paths.get(PROJECT_PATH, "newTest3.txt");
            Path target = Files.copy(destPath1, destPath2, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(target.getFileName());

        } catch (IOException ex) {
            ex.printStackTrace();

        }
    }

    @Test
    public void directory() throws IOException {

        Set<PosixFilePermission> permissions = PosixFilePermissions.fromString("rwxrwx--x");
        FileAttribute<Set<PosixFilePermission>> fileAttributes = PosixFilePermissions.asFileAttribute(permissions);

        Path path = Paths.get(PROJECT_PATH, "Parent", "Child");

        Files.createDirectories(path, fileAttributes);

        Files.createTempDirectory(path, "Concretepage").toFile().deleteOnExit();

    }

    @Test
    public void file() throws IOException {

        Set<PosixFilePermission> permissions = PosixFilePermissions.fromString("rwxrwx--x");
        FileAttribute<Set<PosixFilePermission>> fileAttributes = PosixFilePermissions.asFileAttribute(permissions);

        Path path = Paths.get(PROJECT_PATH, "file.txt");

        Files.createFile(path, fileAttributes);

        Set<PosixFilePermission> posixFilePermissions = new HashSet<>();
        posixFilePermissions.add(PosixFilePermission.OWNER_READ);
        FileAttribute<Set<PosixFilePermission>> fileAttributes2 = PosixFilePermissions.asFileAttribute(posixFilePermissions);

        Path path2 = Paths.get(PROJECT_PATH, "fileNew.txt");
        Files.createFile(path2, fileAttributes2);
    }
}
