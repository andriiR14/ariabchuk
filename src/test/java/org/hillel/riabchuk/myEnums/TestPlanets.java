package org.hillel.riabchuk.myEnums;

import org.junit.Test;

import java.util.Arrays;

public class TestPlanets {

    @Test
    public void RunPlanets() {

        Planet[] planets = Planet.values();
        Arrays.sort(planets, (o1, o2) -> o2.name().compareTo(o1.name()));

        //Arrays.sort(planets, Comparator.comparing(Planet::name));

        for(int i = 0; i < planets.length; i++) {

            System.out.println(planets[i].GetPlanetDescription());
        }
    }
}