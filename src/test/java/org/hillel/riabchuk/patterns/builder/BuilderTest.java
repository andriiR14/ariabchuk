package org.hillel.riabchuk.patterns.builder;

import org.junit.Test;

public class BuilderTest {

    @Test
    public void builder() {

        MealBuilder mealBuilder = new ItalianMealBuilder();
        MealDirector mealDirector = new MealDirector(mealBuilder);
        Meal meal = mealDirector.constructMeal().getMeal();
        System.out.println("meal is: " + meal);

        mealBuilder = new JapaneseMealBuilder();
        mealDirector = new MealDirector(mealBuilder);
        meal = mealDirector.constructMeal().getMeal();
        System.out.println("meal is: " + meal);

        meal = new MealDirector(new JapaneseMealBuilder()).constructMeal().getMeal();
        System.out.println(meal);
    }
}
