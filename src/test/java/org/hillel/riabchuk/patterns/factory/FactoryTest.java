package org.hillel.riabchuk.patterns.factory;

import org.junit.Test;

public class FactoryTest {

    @Test
    public void factory() {

        AnimalFactory animalFactory = new AnimalFactory();

        Animal animal1 = animalFactory.getAnimal("feline");
        System.out.println(animal1.makeSound());

        Animal animal2 = animalFactory.getAnimal("canine");
        System.out.println(animal2.makeSound());
    }
}
