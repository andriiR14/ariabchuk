package org.hillel.riabchuk.patterns.abstractfactory;

import org.hillel.riabchuk.patterns.factory.Animal;
import org.junit.Assert;
import org.junit.Test;

public class AbstractFactoryTest {

    @Test
    public void abstrtactFactory() {

        AbstractFactory abstractFactory = new AbstractFactory();

        SpeciesFactory speciesFactory1 = abstractFactory.getSpeciesFactory("reptile");

        Animal animal1 = speciesFactory1.getAnimal("tyrannosaurus");
        assert animal1.makeSound().equals("Roar!");

        Animal animal2 = speciesFactory1.getAnimal("snake");
        assert animal2.makeSound().equals("Hiss!");

        SpeciesFactory speciesFactory2 = abstractFactory.getSpeciesFactory("mammal");

        Animal animal3 = speciesFactory2.getAnimal("dog");
        Assert.assertEquals("Woof!", animal3.makeSound());

        Animal animal4 = speciesFactory2.getAnimal("cat");
        Assert.assertEquals("Meow!", animal4.makeSound());

        Animal animal = new AbstractFactory()
                .getSpeciesFactory("mammal")
                .getAnimal("dog");

        System.out.println(animal.makeSound());
    }
}
