package org.hillel.riabchuk.functional;


import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class SimpleFunInterfaceTest {

    @Test
    public void test() {

        checkWork(new SimpleFuncInterface() {
            @Override
            public void doWork() {

                System.out.println(" Dp work in SimpleFun impl...");
            }
        });

        checkWork(() -> System.out.println("Do work in lanmbda impl.."));
    }

    public static void checkWork(SimpleFuncInterface simpleFuncInterface) {

        int y = SimpleFuncInterface.x;
        simpleFuncInterface.doWork();
    }

    @Test
    public void predicateTest() {

        Predicate<Integer> isPositive = x -> x > 0;

        System.out.println(isPositive.test(5));
        System.out.println(isPositive.test(0));
        System.out.println(isPositive.test(-4));

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5,6 ,7 ,8, 9);

        System.out.println("Print all numbers");
        eval(list, n -> true);

        System.out.println("Print even numbers:");
        eval(list, n -> n%2 == 0);

        System.out.println("Print numbers greater than 3:");
        eval(list, n -> n > 3);
    }

    public static void eval(List<Integer> list, Predicate<Integer> predicate) {

        for (Integer n: list) {

            if(predicate.test(n)) {
                System.out.println(n + " ");
            }
        }
    }

    @Test
    public void binaryOperator() {

        BinaryOperator<Integer> multiply = (x, y) -> x * y;

        System.out.println(multiply.apply(3, 5));
        System.out.println(multiply.apply(10, -2));
    }

    @Test
    public void function() {

        Function<Integer, String> convert = x -> String.valueOf(x) + " euro";
        System.out.println(convert.apply(5));
    }

    @Test
    public void consumer() {

        Consumer<Integer> printer = x -> {
            Integer integer = new Integer(x + 5);
            System.out.println(integer);
            System.out.printf("%d euro \n", x);
        };

        printer.accept(600);
    }

    @Test
    public void java8ForEachAndMap() {

        //Map<String, Integer> items = new HashMap<>(
    }


//    функ интерф
//    клон
//    лямюда две реализации чтобы скопировать файл с одного места в друг
}
