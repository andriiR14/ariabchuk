package org.hillel.riabchuk.javaeight;

import org.hillel.riabchuk.annotations.MyClass;
import org.junit.Test;

import javax.xml.stream.StreamFilter;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamFilterTest {

    @Test
    public void filter() {

        List<String> lines = Arrays.asList("first", "second", "thirds");

//        List<String> result = lines.stream().filter(l -> !l.equals("second"))
//                .collect(Collectors.toList());

        Stream<String> result1 = lines.stream().filter(l -> !l.equals("second")).map(x -> x.concat("kolobok"));
        result1.forEach(System.out::println);

        lines.forEach(System.out::println);
    }

    @Test
    public void filterSecond() {

        List<Person> persons = Arrays.asList(
                new Person(1, "first", 36),
                new Person(2, "second", 42),
                new Person(3, "thirds", 28));

        Person person1 = persons.stream().parallel()
                .filter(x -> x.getId().equals(2)).findAny().orElse(null);

        System.out.println(person1);

        Person person2 = persons.stream().parallel()
                .filter(x -> x.getId().equals(5)).findAny().orElse(new Person(5, "fifth", 15));

        System.out.println(person2);


    }

    @Test
    public void filterAndMap() {

        List<Person> persons = Arrays.asList(
                new Person(1, "first", 36),
                new Person(2, "second", 42),
                new Person(3, "thirds", 28));

        MyClass myClass = new MyClass() { String name = "kolobok";};

        String name = persons.stream().filter(x -> "second".equals(x.getName()))
                .findAny()
                .map(y-> new MyClass() {

                    Integer id = y.getId();
                    int age = y.getAge();

                    @Override
                    public String toString() {
                        return "$classname{" +
                                "id=" + id +
                                ", age=" + age +
                                '}';
                    }
                })
                .map(z->z.toString())
                .orElse("");

        System.out.println("name " + name);

        List<String> collect = persons.stream().map(Person::toString).collect(Collectors.toList());

        collect.forEach(System.out::println);

        Integer id = persons.stream().parallel().filter(x -> x.getId().equals(8))
                .map(Person::getId)
                .findAny()
                .orElse(null);

        System.out.println("id: " + id);

        Person person = persons.stream().filter(p -> p.getId().equals(id))
                .findAny().orElse(null);

        System.out.println(person);

        //Stream<Integer> integerStream = persons.stream().filter(x -> x.getId() > 1).map(z -> z.getId()).findAny().orElse(null);

    }

    @Test
    public void map() {

        List<String> alpha = Arrays.asList("a", "b", "c", "d");

        //Before Java8
        List<String> alphaUpper = new ArrayList<>();
        for (String s : alpha) {
            alphaUpper.add(s.toUpperCase());
        }

        System.out.println(alpha);
        System.out.println(alphaUpper);

        //Java8
        List<String> collect = alpha.stream().map(String::toUpperCase).collect(Collectors.toList());
        System.out.println(collect);

        List<Integer> num = Arrays.asList(1,2,3,4,5);
        List<Integer> collect1 = num.stream().map(n -> n*2).collect(Collectors.toList());
        System.out.println(collect1);

    }

    @Test
    public void GroupingBy() {

        List<String> items = Arrays.asList("papaya", "papaya", "apple", "banana", "apple", "apple", "orange", "banana", "papaya");

        List<Person> staff = Arrays.asList(
                        new Person(1, "first", 36),
                        new Person(2, "second", 42),
                        new Person(3, "thirds", 28));

        Map<String, Long> results = items.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(results);

        Map<Integer, Long> results2 = staff.stream().map(x -> x.getId())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        System.out.println(results2);

        Map<String, Long> finalMap = new LinkedHashMap<>();

        results.entrySet().stream().sorted(Map.Entry.<String, Long>comparingByKey().reversed())
                .forEach(e -> finalMap.put(e.getKey(), e.getValue()));

        System.out.println(finalMap);

        Stream.of("AAA", "BBB", "CCC").parallel().forEach(s -> System.out.println("Output:" + s));
        System.out.println("--------------------------");
        Stream.of("AAA", "BBB", "CCC").parallel().forEachOrdered(s -> System.out.println("Output:" + s));

    }

    @Test
    public void filterNull() {

        Supplier<Stream<String>> langSupplier = () -> Stream.of("java", "python", "node", null, "ruby", null, "php");

        List<String> result = langSupplier.get().filter(x-> x!=null).collect(Collectors.toList());

        result.forEach(System.out::println);

        System.out.println("-----------------------------------");

        List<String> resultNew = langSupplier.get().filter(Objects::nonNull).collect(Collectors.toList());
        resultNew.forEach(System.out::println);
    }

    @Test
    public void arrayToTest() {

        String[] array = {"a", "b", "c", "d", "e"};

        Stream<String> stream1 = Arrays.stream(array);
        stream1.forEach(System.out::println);

        Stream<String> stream2 = Stream.of(array);
        stream2.forEach(System.out::println);
    }

    @Test
    public void primitiveArrayToStream() {

        int[] array = {1, 2, 3, 4, 5, 6, 7};

        IntStream stream1 = Arrays.stream(array);
        stream1.forEach(System.out::println);

        Stream<int[]> temp = Stream.of(array);
        IntStream stream2 = temp.flatMapToInt(Arrays::stream);
        stream2.forEach(System.out::println);

        //IntStream stream3 = Stream.of(array).flatMapToInt(Arrays::stream);

    }

    @Test
    public void convertsStreamToList() {

        Stream<String> lang = Stream.of("java", "python", "node");

        List<String> result = lang.collect(Collectors.toList());
        result.forEach(System.out::println);

        Stream<Integer> number = Stream.of(1,2,3,4,5,6);
        List<Integer> result2 = number.filter(x->x!=3).collect(Collectors.toList());
        result2.forEach(System.out::println);

        List<Person> persons = Arrays.asList(
                new Person(1, "first", 30),
                new Person(2, "second", 35),
                new Person(3, "third", 45)
        );

        Map<Integer, String> map = mapObj(persons);
        map.forEach((x,y) -> System.out.println(x + " " + y));
        map = persons.stream().collect(Collectors.toMap(Person::getId, Person::getName));
        map.forEach((x,y) -> System.out.println(x + " " + y));

        //number.mapToInt(x->x + 2).forEach(System.out::println);

    }

    public Map<Integer, String> mapObj(List<Person> personList) {

        Map<Integer, String> map = new HashMap<>();
        personList.forEach(x->map.put(x.getId(), x.getName()));
        return map;
    }
}
