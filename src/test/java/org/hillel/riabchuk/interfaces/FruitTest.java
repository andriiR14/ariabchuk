package org.hillel.riabchuk.interfaces;

import org.hillel.riabchuk.Interfaces.Fruit;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FruitTest {

    @Test
    public void sortFruitObjects() {

        List<Fruit> fruits = new ArrayList<>();

        Fruit pinapple = new Fruit("Pinapple", "Pinapple Description", 180);
        Fruit apple = new Fruit("Apple", "Apple Description", 100);
        Fruit orange = new Fruit("Orange", "Orange Description", 120);
        Fruit banana = new Fruit("Banana", "Banana Description", 70);
        Fruit apple2 = new Fruit("Apple", "Apple Description", 60);

        fruits.add(pinapple);
        fruits.add(apple);
        fruits.add(orange);
        fruits.add(banana);
        fruits.add(apple2);

        Collections.sort(fruits);

        for (Fruit fruit : fruits) {
            System.out.println(fruit.getName() + " " + fruit.getQuantity());
        }

        System.out.println();
        Collections.sort(fruits, Fruit.fruitComparator);

        for (Fruit fruit : fruits) {
            System.out.println("fruits: " + " : " + fruit.getName() + ", Quantity : " + fruit.getQuantity());
        }
    }

}
