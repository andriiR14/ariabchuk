package org.hillel.riabchuk.regexp;

import org.hillel.riabchuk.regex.UsernameValidator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class UsernameValidatorTest {

    private UsernameValidator usernameValidator;

    @BeforeClass
    public void initData() {
        usernameValidator = new UsernameValidator();
    }

    @DataProvider
    public Object[][] validUsernameProvider() {
        return new Object[][] {{ new String[] {"ale", "alex_2002", "alex-202", "alex3-4_good"}}};
    }

    @Test(dataProvider = "validUsernameProvider")
    public void  validUsernameTest(String[] username) {

        for(String temp : username) {
            boolean valid = usernameValidator.validate(temp);
            System.out.println("Username is valid : " + temp + " , " + valid);
            Assert.assertEquals(true, valid);
        }
    }

    // @Test(dataProvider = "validUsernameProvider", dependsOnMethods = "validUsernameTest")
    public void inValidUsernameTest(String[] username) {

        for(String temp : username) {
            boolean valid = usernameValidator.validate(temp);
            System.out.println("Username is invalid : " + temp + " , " + valid);
            Assert.assertEquals(false, valid);
        }
    }

    // дома !+-.ji*#/\[]{}?a-zA-Z0-9
    // 13 - 25

}