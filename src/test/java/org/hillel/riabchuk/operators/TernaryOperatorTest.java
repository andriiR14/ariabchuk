package org.hillel.riabchuk.operators;

import org.junit.Test;

import static org.hillel.riabchuk.operators.TernaryOperator.*;

public class TernaryOperatorTest {

    @Test
    public void ternary() {

        String str = "Australia";
        int i = 12;

        System.out.println(getMinValue(4, 10));
        System.out.println(getAbsoluteVal(-15));
        System.out.println(invertBoolean(false));

        containsA(str);
        ternaryMethod(i, new TernaryOperator());
    }
}
