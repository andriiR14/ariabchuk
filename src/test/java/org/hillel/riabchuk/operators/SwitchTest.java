package org.hillel.riabchuk.operators;

import org.junit.Test;

public class SwitchTest {

    @Test
    public void TestSwitchOperator() {

        int[] vals = new int[] {10, 5, 7};
        for (int val: vals) {
            Switch.switchMethod(val);
        }

    }
}
