package org.hillel.riabchuk.loopsandarrays;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hillel.riabchuk.loopsandarrays.ForLoopsAndArrays.*;

public class ForLoopsAndArraysTest {

    @Test
    public void checkingEqualityOfTwoArray() {

        int[] ar1 = null;
        int[] ar2 = {2, 5, 7, 8, 11};
        int[] ar3 = {2, 5, 7, 9, 11};
        int[] ar4 = {2, 5, 7, 8, 11};
        int[] ar5 = null;
        int[] ar6 = new int[0];
        int[] ar7 = new int[0];

        if (equalityOfTwoArrays(ar1, ar2)) {
            System.out.println("Two arrays are equal");
        }
        else {
            System.out.println("Two arrays are not equal");
        }
    }

    @Test
    public void checkUniqArray() {

        int[] ar1 = {2, 5, 7, 5, 11, 17, 2};
        int[] ar2 = {2, 5, 7, 7, 2, 5, 2};
        uniqArray(ar1);
        System.out.println();
        System.out.println();
        uniqArray(ar2);
    }

    @Test
    public void testJava8() {

        List<Integer> list = new ArrayList<>();
        list.add(5);
        list.add(15);
        list.add(25);
        list.add(-5);

        for (Integer i : list)
            System.out.println(i);

        list.forEach(System.out::println);

        list.forEach(x->System.out.println(x));
    }

}
