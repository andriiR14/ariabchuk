package org.hillel.riabchuk.loopsandarrays;

import org.junit.Test;

import java.text.DecimalFormat;

import static org.hillel.riabchuk.loopsandarrays.Matrix.*;

public class MatrixTest {

    @Test
    public void checkMatrixMultiplication() {

        Double[][] first = new Double[][] {{4.00,3.00, 5.67},
                                        {2.00,1.00, 5.67},
                                        {5.43,4.12, 5.67}};
        Double[][] second = new Double[][] {{-0.50, 1.50, 5.67},
                                        {1.00, -2.000, 5.67},
                                        {3.23, 6.15, 5.67}};

        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        Double[][] result = multiplicar(first, second);
        for (Double[] r1 : result) {
            for (Double r2 : r1) {
                System.out.print(df.format(r2) + "\t\t");
            }
            System.out.println();
        }
    }
}
