package org.hillel.riabchuk.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsernameValidator {

    private Pattern pattern;
    private Matcher matcher;

    /**
     * ^ - Start of the line
     * [a-z0-9_-] - Match characters and symbols in the list, a-z, 0-9, underscore, hyphen
     * {3,15} - Length at least 3 charac
     */

    private static final String USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$";;

    public UsernameValidator() {
        pattern = Pattern.compile(USERNAME_PATTERN);
    }

    public boolean validate(String username) {
        matcher = pattern.matcher(username);
        return matcher.matches();
    }
}