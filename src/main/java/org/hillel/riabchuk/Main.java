package org.hillel.riabchuk;

import org.hillel.riabchuk.firstapp.FirstApp;
import org.hillel.riabchuk.operators.Switch;
import org.hillel.riabchuk.operators.TernaryOperator;

public class Main {

    public static void main(String [ ] args) {
        new FirstApp();
        new Switch();
        new TernaryOperator();

    }

}
