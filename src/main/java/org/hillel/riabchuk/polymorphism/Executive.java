package org.hillel.riabchuk.polymorphism;

public class Executive extends Employee {

    private double bonus;

    public Executive(String name, String address, String phone, String socialSecurityNubmer, double payRate) {
        super(name, address, phone, socialSecurityNubmer, payRate);
        this.bonus = 0.0;
    }

    public void awardBonus(double execBonus) {
        bonus = execBonus;
    }

    @Override
    public double pay() {
        double payment = super.pay() + bonus;
        bonus = 0;
        return payment;
    }
}
