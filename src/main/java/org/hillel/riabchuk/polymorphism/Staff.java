package org.hillel.riabchuk.polymorphism;

import java.util.ArrayList;
import java.util.List;

public class Staff {

    List<StaffMember> staffList;

    public Staff() {

        staffList = new ArrayList<>();

        staffList.add(new Executive("Sam", "123 Main Line", "555-2103", "2486-5289", 2485.65));
        staffList.add(new Employee("Paul", "25 First Line", "526-0432", "5987-3547", 1475.95));
        staffList.add(new Employee("Andy", "333 Test Line", "778-5789", "3654-3265", 3485.66));
        staffList.add(new Hourly("Alex", "5 Common Line", "665-5214", "7416-9612", 24.58));
        staffList.add(new Volunteer("Lior", "89 Lior Line", "895-8957"));
        staffList.add(new Volunteer("Tom", "14 Second Line", "369-1111"));

        for (StaffMember sm: staffList) {
            if (sm instanceof Executive)
                ((Executive) sm).awardBonus(500);
            else if (sm instanceof Hourly)
                ((Hourly) sm).addHours(40);
        }
    }

    public void payDay() {

        double amount;

        for (StaffMember sm : staffList) {
            System.out.println(sm);
            amount = sm.pay();

            if (amount == 0.0) {
                System.out.println("Thanks!");
            }
            else {
                System.out.println("Paid: " + amount);
            }
            System.out.println("-----------------------------------");
        }

    }
}
