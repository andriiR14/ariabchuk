package org.hillel.riabchuk.polymorphism;

public class Employee extends StaffMember {

    private String socialSecurityNubmer;
    private double payRate;

    public Employee(String name, String address, String phone, String socialSecurityNubmer, double payRate) {
        super(name, address, phone);
        this.socialSecurityNubmer = socialSecurityNubmer;
        this.payRate = payRate;
    }

    @Override
    public String toString() {
        return super.toString() + "\nsocialSecurityNubmer: " + socialSecurityNubmer;
    }

    @Override
    public double pay() {
        return payRate;
    }
}
