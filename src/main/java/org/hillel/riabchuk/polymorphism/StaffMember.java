package org.hillel.riabchuk.polymorphism;

public abstract class StaffMember {

    private String name;
    private String address;
    private String phone;

    public StaffMember(String name, String address, String phone) {
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "name: " + name + '\n' +
                "address: " + address + '\n' +
                "phone:" + phone;
    }

    public abstract double pay();
}
