package org.hillel.riabchuk.polymorphism;

public class Hourly extends Employee {

    private int hourWorked;

    public Hourly(String name, String address, String phone, String socialSecurityNubmer, double payRate) {
        super(name, address, phone, socialSecurityNubmer, payRate);
        this.hourWorked = 8;
    }

    public void addHours(int moreHours) {
        hourWorked += moreHours;
    }

    @Override
    public double pay() {
        double payment = super.pay() * hourWorked;
        hourWorked = 0;
        return payment;
    }

    @Override
    public String toString() {
        return super.toString() + "\nCurrent hours: " + hourWorked;
    }
}
