package org.hillel.riabchuk.multithreading;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorsTest {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.submit(() -> {

            String threadName = Thread.currentThread().getName();

            try {
                TimeUnit.SECONDS.sleep(4);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            System.out.println("Hello " + threadName);
        });

        try {

            System.out.println("attept to shutdown executor");
            Thread.sleep(10000);
            executor.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

            System.err.println("task interrupted");
        } finally {

            if(!executor.isTerminated()) {
                System.err.println("cancel non-finished tasks");
            }

            executor.shutdown();
            System.out.println("shutdown finished");
        }
    }


}