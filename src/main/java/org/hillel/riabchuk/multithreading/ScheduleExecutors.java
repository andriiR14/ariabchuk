package org.hillel.riabchuk.multithreading;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ScheduleExecutors {

    public static void main(String[] args) {

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

        Runnable task = () -> System.out.println("Scheduling1: " + System.currentTimeMillis());

        ScheduledFuture<?> future = executor.schedule(task, 3, TimeUnit.SECONDS);

        System.out.println(System.currentTimeMillis());

        long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
        System.out.printf("Remaining Delay: %s", remainingDelay);
        System.out.println();
        executor.shutdown();

        System.out.println("Executor2");
        ScheduledExecutorService executor2 = Executors.newScheduledThreadPool(1);

        Runnable task2 = () -> {
            try {
                TimeUnit.SECONDS.sleep(2);
                System.out.println("Scheduling2: " + System.currentTimeMillis());
            } catch (InterruptedException e) {
                System.err.println("Task interrupted");
            }
        };

        executor2.scheduleWithFixedDelay(task2, 0, 1, TimeUnit.SECONDS);
    }
}
