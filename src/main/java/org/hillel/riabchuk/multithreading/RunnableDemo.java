package org.hillel.riabchuk.multithreading;

import java.util.concurrent.atomic.AtomicInteger;

public class RunnableDemo implements Runnable {

    private Thread t;
    private volatile String threadName;
    private static volatile Integer test = 0;
    static  Integer x = 0;
    static AtomicInteger y = new AtomicInteger(10);

    public RunnableDemo(String name) {
        threadName = name;
        System.out.println("Creating " + threadName);
    }

    @Override
    public void run() {

        System.out.println("Running " + threadName);

        try {

            for (int i = 4; i > 0; i--) {
                System.out.println("Thread " + threadName + ", " + i);
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread " + threadName + " interrupted.");
        }

        System.out.println("Thread " + threadName + " exiting.");
        add();
    }

    public static void add() {
        test++;

        synchronized (x) {
            x++;
            System.out.println(x);
        }

        y.incrementAndGet();
        System.out.println(y);
        System.out.println(test + "Volatile");
    }

    public synchronized void start() {
        System.out.println("Starting " + threadName);

        if(t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}