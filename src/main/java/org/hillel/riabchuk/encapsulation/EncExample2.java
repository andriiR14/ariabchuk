package org.hillel.riabchuk.encapsulation;

public class EncExample2 {

    public void test() {

        EncapsulationExample enc = new EncapsulationExample();

        System.out.println(enc.x);
        System.out.println(enc.z);
        System.out.println(enc.q);
    }
}
