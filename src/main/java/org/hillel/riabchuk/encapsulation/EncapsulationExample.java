package org.hillel.riabchuk.encapsulation;

public class EncapsulationExample {

    public int x = 5;
    private int y = 10;
    protected int z = 15;
    int q = 20;

    public void test() {
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(q);
    }
}
