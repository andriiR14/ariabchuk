package org.hillel.riabchuk.utils;

import org.junit.Assert;

public class SoftAssert {

    public static void assertEquals(Object o1, Object o2) {
        try {
            Assert.assertEquals(o1, o2);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println();
            System.out.println(e.toString());
        }
    }
}
