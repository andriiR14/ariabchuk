package org.hillel.riabchuk.firstapp;

import static java.lang.System.*;

public class FirstApp {

    public FirstApp() {}

    /**
     * generic print
     * @param t value to print
     * @param <T> type
     */
    public static<T> void print(T t) {
        out.print(t);
    }

    /**
     * print object
     * @param object
     */
    public static void println(Object object) {
        out.println(object);
    }

    public static void println() {
        out.println();
    }
}
