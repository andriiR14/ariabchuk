package org.hillel.riabchuk.inner;

public class Outer {

    private static String textNested = "Nested String!";
    private String text = "I am private!";

    public static class Nested {


        public void printNestedText() {

            System.out.println(textNested);
        }

        public static void printStaticNestedText() {
            System.out.println(textNested);
        }
    }

    public class Inner {

        public String text = "I'm Inner private";

        public void printText() {
            System.out.println(text);
            System.out.println(Outer.this.text);
            System.out.println(Inner.this.text);
        }

        public void printStaticText() {
            System.out.println(textNested);
        }
    }

    public void printClasses() {

        Outer.Nested nested = new Outer.Nested();
        nested.printNestedText();
        Outer.Nested.printStaticNestedText();

        Inner inner = new Inner();
        inner.printText();
        inner.printStaticText();
    }

    public void local() {

        class Local {

            private int x;

            public Local() {x = 5;}

            public Local(int x) {this.x = x;}

            int getX() {return this.x;}

            Local getNewLocal() {return new Local();}

            Local getCurrentLocal() {return this;}
        }

        Local local = new Local(10);
        System.out.println(local.getX());
        System.out.println(local.getNewLocal().getX());
        System.out.println(local.getCurrentLocal().getX());

    }

    public void doIt() {
        System.out.println("Do It");
    }

}
