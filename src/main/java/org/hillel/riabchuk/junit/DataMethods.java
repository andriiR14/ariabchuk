package org.hillel.riabchuk.junit;

import java.io.FileNotFoundException;
import java.io.StreamTokenizer;
import java.util.StringTokenizer;

public class DataMethods {

    public static int sum(int x, int y) {
        return x+y;
    }

    public static int multiply(int x, int y) {
        return x*y;
    }

    public static int findMax(int arr[]) throws FileNotFoundException {
        int max = arr[0];

        for (int i = 0; i < arr.length; i++) {
            if (max < arr[i])
                max = arr[i];
        }

        return max;
    }

    public static int cube(int n) {
        return n*n*n;
    }

    public static String reverseWord(String str) {

        StringBuilder result = new StringBuilder();
        StringTokenizer tokenizer = new StringTokenizer(str, " ");
        while (tokenizer.hasMoreTokens()) {
            StringBuilder sb = new StringBuilder();
            sb.append(tokenizer.nextToken());
            sb.reverse();

            result.append(sb);
            result.append(" ");
        }
        return result.toString();
    }
}
