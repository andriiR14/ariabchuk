package org.hillel.riabchuk.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class NewExcel {

    public static void main(String[] args) {

        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet spreadsheet = workbook.createSheet("Employee Info");

        Map<String, Object[]> empinfo = new TreeMap();

        empinfo.put("6", new Object[]{"tp05", "Oleg", "Technical Writer"});
        empinfo.put("1", new Object[]{"EMP ID", "EMP NAME", "DESIGNATION"});
        empinfo.put("2", new Object[]{"tp01", "Vovan", "Technical Manager"});
        empinfo.put("3", new Object[]{"tp02", "Ivan", "Proof Reader"});
        empinfo.put("4", new Object[]{"tp03", "Jack", "Technical Writer"});
        empinfo.put("5", new Object[]{"tp05", "Leo", "Technical Writer"});

        int rowid = 0;

        for (Map.Entry entry : empinfo.entrySet()) {

            XSSFRow row = spreadsheet.createRow(rowid++);
            int cellid = 0;

            for (Object obj : (Object[]) entry.getValue()) {

                Cell cell = row.createCell(cellid++);
                ((XSSFCell) cell).setCellValue((String) obj);
            }
        }

        try (FileOutputStream out = new FileOutputStream(new File("Writesheet.xlsx"))) {

            workbook.write(out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Writesheet.xlsx written successfully");
    }
}
