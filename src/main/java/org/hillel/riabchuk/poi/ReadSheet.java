package org.hillel.riabchuk.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class ReadSheet {

    public static void main(String[] args) {

        try (FileInputStream fis = new FileInputStream(new File("Writesheet.xlsx"))) {

            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            XSSFSheet spreadsheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = spreadsheet.iterator();

            while (rowIterator.hasNext()) {

                XSSFRow row = (XSSFRow) rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext()) {

                    Cell cell = cellIterator.next();

                    switch (cell.getCellTypeEnum()) {
                        case NUMERIC:
                            System.out.print(cell.getNumericCellValue() + "\t\t ");
                            break;
                        case STRING:
                            System.out.print(cell.getStringCellValue() + "\t\t ");
                            break;
                        case BLANK:
                            break;
                        default:
                            throw new AssertionError();
                    }
                }

                System.out.println();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}