package org.hillel.riabchuk.maps;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


public class ReadSheet {

    public static void main(String[] args) {
        try(FileInputStream fileInputStream = new FileInputStream(new File("target/Writesheet.xlsx"))) {
            for (Row row : new XSSFWorkbook(fileInputStream).getSheetAt(0)) {
                for (Cell cell : row) {
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_NUMERIC:
                            System.out.print(cell.getNumericCellValue() + " \t\t ");
                            break;
                        case Cell.CELL_TYPE_STRING:
                            System.out.print(cell.getStringCellValue() + " \t\t ");
                            break;
                        default:
                            throw new AssertionError();
                    }
                }
                System.out.println();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
