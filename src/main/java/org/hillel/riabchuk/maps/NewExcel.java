package org.hillel.riabchuk.maps;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class NewExcel {

    public static void main(String[] args) {
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet spreadSheet = workbook.createSheet(" Employee Info ");

        Map<String, Object[]> empinfo = new TreeMap<>();

            empinfo.put("1", new Object[] {"EMP ID", "EMP NAME", "DESIGNATION"});
            empinfo.put("2", new Object[] {"tp01", "Vovan", "Technical Manager"});
            empinfo.put("3", new Object[] {"tp02", "Ivan", "Proof Reader"});
            empinfo.put("4", new Object[] {"tp03", "Jack", "Technical Writer"});
            empinfo.put("5", new Object[] {"tp04", "Leo", "Technical Writer"});
            empinfo.put("6", new Object[] {"tp05", "Oleg", "Technical Writer"});

        Set<String> keyid = empinfo.keySet();

        XSSFRow row;

        int rowid = 0;

        for (String key : keyid) {
            row = spreadSheet.createRow(rowid++);
            Object[] objects = empinfo.get(key);
            int celling = 0;
            for (Object obj : objects) {
                Cell cell = row.createCell(celling++);
                cell.setCellValue(obj.toString());
            }
        }

        try (FileOutputStream out = new FileOutputStream(new File("target/Writesheet.xlsx"))) {
            workbook.write(out);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("Writesheet.xlss written successfully");
    }
}
