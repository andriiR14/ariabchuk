package org.hillel.riabchuk.functional;

@FunctionalInterface
public interface FirstFuncInterface<T, R> {

    T get(R r);

    //void smth();

    default void test() {
        System.out.println("test1");
    }
}
