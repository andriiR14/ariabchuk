package org.hillel.riabchuk.functional;

public class TestMultipleImpl {



    public static void main(String[] args) {

        FirstFuncInterface<String, Integer> firstFuncInterface = (x) -> {Integer y = x * x; return y.toString();};

        System.out.println(firstFuncInterface.get(5));

        SecondFuncInterface<String> secondFuncInterface = (y) -> {
            System.out.println(y + " + some part of string");
        };

        secondFuncInterface.smth("kolobok");
    }

}
