package org.hillel.riabchuk.functional;

@FunctionalInterface
public interface ComplexFuncInterface extends SimpleFuncInterface {

    int x = 5;

    static void doSomeWork() {
        System.out.println("Doing some work in interface impl...");
    }

    @Override
    default void doAnotherWork() {
        System.out.println("Doing another work in interface impl...");
    }

    @Override
    void doWork();

    @Override
    String toString();

    @Override
    boolean equals(Object o);
}
