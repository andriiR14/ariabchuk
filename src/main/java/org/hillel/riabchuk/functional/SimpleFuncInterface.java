package org.hillel.riabchuk.functional;

@FunctionalInterface
public interface SimpleFuncInterface {

    int x = 5;

    static void doSomeWork() {
        System.out.println("Doing some work in interface impl...");
    }

    default void doAnotherWork() {
        System.out.println("Doing another work in interface impl...");
    }

    void doWork();
    String toString();
    boolean equals(Object o);
}
