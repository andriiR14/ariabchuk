package org.hillel.riabchuk.functional;

@FunctionalInterface
public interface SecondFuncInterface<T> {

    void smth(T t);

    default void test() {
        System.out.println("test2");
    }
}
