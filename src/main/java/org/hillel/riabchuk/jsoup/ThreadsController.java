package org.hillel.riabchuk.jsoup;

import org.apache.commons.io.FilenameUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.*;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ThreadsController implements Runnable {

    private String url;

    private int counter;
    private Path path = Paths.get(".").toAbsolutePath().getParent();
    private static boolean DEBUG;

    public ThreadsController(String url) {
        this.url = url;
    }

    @Override
    public void run() {

        try {

            File linkFile = new File(path + "/src/main/resources/links/", FilenameUtils.getName(new URL(url).getPath()));
            System.out.println(url);
            //try (BufferedWriter linksFileWriter = new BufferedWriter(new FileWriter(linkFile, false))) {
            try (FileOutputStream linksFileWriter = new FileOutputStream(linkFile)) {

                Connection.Response html = Jsoup.connect(url).ignoreContentType(true).execute();
                linksFileWriter.write(html.bodyAsBytes());

                if(DEBUG == true)
                    System.out.println(Thread.currentThread().getName() + " Started");

                counter = TestMain.atomicInteger.incrementAndGet();
                System.out.println(counter + " number of Sites are checked");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
