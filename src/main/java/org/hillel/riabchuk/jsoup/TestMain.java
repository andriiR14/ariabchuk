package org.hillel.riabchuk.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

public class TestMain {

    public static AtomicInteger atomicInteger = new AtomicInteger(0);

    public static void main(String[] args) throws Exception {

        String mainLink = "http://santehshop.kiev.ua/"; //"http://websystique.com/";

        List<String> links = new ArrayList<>();

        Document document = Jsoup.connect(mainLink).get();
        //Elements linkTags = document.select("a[href]");

        Elements imgs = document.getElementsByTag("img");

        Integer threadQueantity = 10;//linkTags.size()-1;
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadQueantity);

        String link;
        for (Element element : imgs) {
            link = element.absUrl("src");
            links.add(link);
        }
        for (int i = 0; i < threadQueantity; i++) {
            ThreadsController threadsController = new ThreadsController(links.get(i));
            executor.execute(threadsController);
        }
        executor.shutdown();
    }
}
