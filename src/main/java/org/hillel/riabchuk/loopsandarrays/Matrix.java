package org.hillel.riabchuk.loopsandarrays;

public class Matrix {

    Integer id;

    public static Double[][] multiplicar(Double[][] first, Double[][] second) {

        int firstColumn = first.length;
        int firstRow = first[0].length;
        int secondColumn = second.length;
        int secondRow = second[0].length;

        if (firstRow != secondColumn) {
            throw new IllegalArgumentException("First Row: " + firstRow + " did not match Second Columns " + secondColumn + ".");
        }

        Double[][] result = new Double[firstRow][secondColumn];
        for (int i = 0; i < firstColumn; i++) {
            for (int j = 0; j < secondRow; j++) {
                result[i][j] = 0.00000;
            }
        }

        for (int i = 0; i < firstRow; i++) {
            for (int j = 0; j < secondColumn; j++) {
                for (int k = 0; k < result.length; k++) {
                    result[i][j] += first[i][k] * second[k][j];
                }
            }
        }

        return result;
    }
}
