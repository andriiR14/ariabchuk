package org.hillel.riabchuk.loopsandarrays;

import java.util.Arrays;

public class ForLoopsAndArrays {

    public static boolean equalityOfTwoArrays(int[] ar1, int[] ar2) {

        boolean equalOrNull = true;

        if ((ar1 != null && ar2 != null) && ar1.length == ar2.length) {
            for (int i = 0; i < ar1.length; i++) {
                if (ar1[i] != ar2[i]) {
                    equalOrNull = false;
                    break;
                }
            }
        }
        else if (ar1 != null || ar2 != null)
            equalOrNull = false;

        return equalOrNull;
    }

    public static void uniqArray(int[] ar) {

        int arraySize = ar.length;

        System.out.println("Original array: ");

        for (int i = 0; i < arraySize; i++)
            System.out.print(ar[i] + "\t");

        for (int i = 0; i < arraySize; i++) {

            for (int j = i + 1; j < arraySize; j++) {

                if (ar[i] == ar[j]) {
                    ar[j] = ar[arraySize - 1];
                    arraySize--;
                    j--;
                }
            }
        }

        int[] ar1 = Arrays.copyOf(ar, arraySize);
        System.out.println();

        System.out.println("Array with unique values");

        for (int i = 0; i < arraySize; i++)
            System.out.print(ar1[i] + "\t");
    }
}
