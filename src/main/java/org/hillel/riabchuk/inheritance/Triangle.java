package org.hillel.riabchuk.inheritance;

import org.junit.Test;

import java.util.List;

public class Triangle extends Poligon {

    public Triangle() {
        super();
        for (int i = 0; i < 3; i++)
            super.getSides().add(1.0);
    }

    public Triangle(double sideA, double sideB, double sideC) {
        super.getSides().add(sideA);
        super.getSides().add(sideB);
        super.getSides().add(sideC);
    }

    public double getSide(int sideIndex) {
        return super.getSides().get(sideIndex);
    }

    @Override
    public double getArea() {
        double result = getPerimeter();
        for (int i = 0; i < getSides().size(); i++)
            result *= (getPerimeter() - getSides().get(i));
        return Math.sqrt(result);
    }

    @Override
    public String toString() {
        return "Triangle: \n" + super.toString();
    }
}
