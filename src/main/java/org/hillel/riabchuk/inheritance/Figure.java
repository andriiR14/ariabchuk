package org.hillel.riabchuk.inheritance;

public abstract class Figure {

    abstract double getArea();

}
