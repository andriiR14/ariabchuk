package org.hillel.riabchuk.inheritance;

import java.util.ArrayList;
import java.util.List;

public abstract class Poligon extends Figure {

    private List<Double> sides = new ArrayList<>();

    public List<Double> getSides() {
        return sides;
    }

    public void setSides(List<Double> sides) {
        this.sides = sides;
    }

    public double getPerimeter() {
        double perim = 0;
        for (double side : sides) {
            perim += side;
        }

        return perim;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= sides.size(); i ++)
            sb.append("side" + i + ": " + sides.get(i-1) + "\n");
        sb.append("Perimeter: " + getPerimeter());
        return sb.toString();
    }

    @Override
    public double getArea() {
        return 0;
    }
}
