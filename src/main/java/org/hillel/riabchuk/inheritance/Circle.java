package org.hillel.riabchuk.inheritance;

import java.util.concurrent.CyclicBarrier;

public class Circle extends Figure{

    private double radius;

    public Circle() {
        this.radius = 1.0;
    }

    public Circle(double r) {
        this.radius = r;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return calculateArea();
    }

    private double calculateArea() {
        return this.radius * this.radius * Math.PI;
    }

    @Override
    public String toString() {
        return "The Radius of The Circle: " + radius;
    }
}
