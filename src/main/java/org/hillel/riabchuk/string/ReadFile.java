package org.hillel.riabchuk.string;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ReadFile {

    public static List<Employee> readCSV(String path) {

        List<Employee> employeeList = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(path))) {

            String line;

            while ((line = br.readLine()) != null) {
                System.out.println(line);

                StringTokenizer stringTokenizer = new StringTokenizer(line, ";");

                while (stringTokenizer.hasMoreElements()) {

                    Integer id = Integer.parseInt(stringTokenizer.nextToken());
                    BigDecimal salary = new BigDecimal(Double.parseDouble(stringTokenizer.nextElement().toString())).setScale(2, RoundingMode.HALF_UP);
                    String username = stringTokenizer.nextElement().toString();

                    StringBuilder sb = new StringBuilder();
                    sb.append("\nId :" + id);
                    sb.append("\nSalary :" + salary);
                    sb.append("\nUsername :" + username);
                    sb.append("\n*******************");

                    System.out.println(sb.toString());

                    employeeList.add(new Employee(id, username, salary));
                }

            }
            System.out.println("Done");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return employeeList;
    }
}
