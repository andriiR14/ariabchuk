package org.hillel.riabchuk.string;

import java.math.BigDecimal;

public class Employee {

    private String name;
    private Integer id;
    private BigDecimal salary;

    public Employee(Integer id, String name, BigDecimal salary) {
        this.name = name;
        this.id = id;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer age) {
        this.id = age;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id='" + id + '\'' +
                ", name=" + name +
                ", salary=" + salary +
                '}';
    }
}
