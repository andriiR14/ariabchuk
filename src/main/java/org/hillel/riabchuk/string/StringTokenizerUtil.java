package org.hillel.riabchuk.string;

import java.util.StringTokenizer;

public class StringTokenizerUtil {

    public static void splitBySpace(String str) {

        StringTokenizer st = new StringTokenizer(str);

        System.out.println("----Split by space----");
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }

    public static void splitByComma(String str) {

        StringTokenizer st = new StringTokenizer(str, ",");

        System.out.println("----Split by comma----");
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }
}
