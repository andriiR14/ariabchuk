package org.hillel.riabchuk.operators;

public class TernaryOperator {

    private final Integer FIRST = 5;
    private final Integer SECOND = 10;
    private String str;

    public TernaryOperator() {}

    public Integer getFIRST() {
        return FIRST;
    }

    public Integer getSECOND() {
        return SECOND;
    }

    public static int getMinValue(int i, int j) {
        return (i < j) ? i : j;
    }

    public static int getAbsoluteVal(int i) {
        return i < 0 ? -i : i;
    }

    public static boolean invertBoolean(boolean b) {
        return b ? false : true;
    }

    public static void containsA(String str) {
        String data = str.contains("A") ? "Str contains 'A'" : "Str doesn't contain 'A'";
    }

    public static void ternaryMethod(Integer i, TernaryOperator ternaryOperator) {
        System.out.println((i.equals(ternaryOperator.getFIRST()))
            ? "i=5" : ((i.equals(ternaryOperator.getSECOND()))) ? "i=10" : "i is not equal to 5 or 10");
    }
}
