package org.hillel.riabchuk.operators;

public class Switch {

    public static void switchMethod(Integer x) {

        final int firstCase = 5;
        final int secondCase = 10;

        switch (x) {
            case firstCase:
                System.out.println("i=5");
                break;
            case secondCase:
                System.out.println("i=10");
                break;
            default:
                System.out.println("i is not equal to 5 or 10");
                break;
        }
    }
}
