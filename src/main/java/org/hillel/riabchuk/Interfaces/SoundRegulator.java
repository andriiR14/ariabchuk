package org.hillel.riabchuk.Interfaces;

public interface SoundRegulator {

    void up();
    void down();

}
