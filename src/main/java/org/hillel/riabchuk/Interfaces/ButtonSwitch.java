package org.hillel.riabchuk.Interfaces;

public interface ButtonSwitch {

    void switchON();
    void switchOFF();

}
