package org.hillel.riabchuk.Interfaces;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Fruit implements Comparable<Fruit> {

    private String name;
    private String description;
    private int quantity;

    public Fruit(String name, String description, int quantity) {
        this.name = name;
        this.description = description;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int compareTo(Fruit o) {
        return this.quantity - o.getQuantity() ;
    }

    List<String> strings = new ArrayList<>();

    public static Comparator<Fruit> fruitComparator = new Comparator<Fruit>() {

        @Override
        public int compare(Fruit fruit1, Fruit fruit2) {
            String f1 = fruit1.getName().toUpperCase();
            String f2 = fruit2.getName().toUpperCase();

            return f1.compareTo(f2);
        }
    };

}
