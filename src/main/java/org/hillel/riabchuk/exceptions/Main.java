package org.hillel.riabchuk.exceptions;

public class Main {

    public static void main(String[] args) {

        try {
            CustomerService.findByName("");
        }
        catch (NameNotFoundException ex) {
            try {
                CustomerService.findByName("Name");
            }
            catch (NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
