package org.hillel.riabchuk.exceptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class AppZip {

    List<String> fileList;
    private static final String OUTPUT_ZIP_FILE = "/home/javaelizarenko/IdeaProjects/javaelem/ariabchuk/src/main/resources/MyZip.zip";
    private static final String SOURCE_FOLDER = "/home/javaelizarenko/IdeaProjects/javaelem/ariabchuk/src/main/resources";

    public AppZip() {
        fileList = new ArrayList<>();
    }

    public static void main(String[] args) {

        AppZip appZip = new AppZip();
        appZip.generateFileList(new File(SOURCE_FOLDER));
        appZip.zipIt(OUTPUT_ZIP_FILE);
    }

    public void generateFileList(File node) {

        if (node.isFile()) {
            fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
        }

        if (node.isDirectory()) {
            String[] sudNote = node.list();
            for (String filename : sudNote) {
                generateFileList(new File(node, filename));
            }
        }
    }
    
    private String generateZipEntry(String file) {
        return file.substring(SOURCE_FOLDER.length() + 1, file.length());
    }

    public void zipIt(String zipFile) {

        byte[] buffer = new byte[1024];

        try (FileOutputStream fos = new FileOutputStream(zipFile);
             ZipOutputStream zos = new ZipOutputStream(fos)) {

            System.out.println("Output to Zip : " + zipFile);

            for (String file : this.fileList) {

                ZipEntry ze = new ZipEntry(file);
                zos.putNextEntry(ze);

                try (FileInputStream in = new FileInputStream(SOURCE_FOLDER + File.separator + file)) {

                    int len;
                    while ((len = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }

                    System.out.println("File Added : " + file);
                }
                catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            fos.flush();
            zos.closeEntry();

            System.out.println("Done");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
