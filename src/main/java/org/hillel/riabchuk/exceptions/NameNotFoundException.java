package org.hillel.riabchuk.exceptions;

public class NameNotFoundException extends Exception {

    private int errorCode;

    public NameNotFoundException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
