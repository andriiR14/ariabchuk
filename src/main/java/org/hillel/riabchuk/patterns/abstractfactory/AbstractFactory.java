package org.hillel.riabchuk.patterns.abstractfactory;

public class AbstractFactory {

    public SpeciesFactory getSpeciesFactory(String type) {

        if ("mammal".equalsIgnoreCase(type))
            return new MammalFactory();
        else
            return new ReptileFactory();
    }
}
