package org.hillel.riabchuk.patterns.abstractfactory;

import org.hillel.riabchuk.patterns.factory.Animal;

public abstract class SpeciesFactory {

    public abstract Animal getAnimal(String type);
}
