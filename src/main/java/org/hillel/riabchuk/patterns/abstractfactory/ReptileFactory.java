package org.hillel.riabchuk.patterns.abstractfactory;

import org.hillel.riabchuk.patterns.factory.*;

public class ReptileFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) {

        if ("snake".equalsIgnoreCase(type))
            return new Snake();
        else
            return new Tyrannosaurus();
    }
}
