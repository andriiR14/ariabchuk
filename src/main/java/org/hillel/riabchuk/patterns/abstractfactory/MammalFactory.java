package org.hillel.riabchuk.patterns.abstractfactory;

import org.hillel.riabchuk.patterns.factory.*;

public class MammalFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) {

        if ("dog".equalsIgnoreCase(type))
            return new Dog();
        else
            return new Cat();
    }
}
