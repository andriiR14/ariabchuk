package org.hillel.riabchuk.patterns.singleton;

public class SingletonExample {

    private static SingletonExample instance = null;

    private SingletonExample() {

    }

    public static SingletonExample getInstance() {

        if (instance == null)
            instance = new SingletonExample();
        return instance;
    }

    public void sayHello() {
        System.out.println("Hello");
    }
}
