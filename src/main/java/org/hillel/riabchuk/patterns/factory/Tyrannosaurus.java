package org.hillel.riabchuk.patterns.factory;

public class Tyrannosaurus extends Animal {

    @Override
    public String makeSound() {
        return "Roar!";
    }
}
