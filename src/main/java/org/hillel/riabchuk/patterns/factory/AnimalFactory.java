package org.hillel.riabchuk.patterns.factory;

public class AnimalFactory {

    public Animal getAnimal(String type) {

        if ("canine".equalsIgnoreCase(type)) {
            return new Dog();
        } else {
            return new Cat();
        }
    }
}
