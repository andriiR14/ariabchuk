package org.hillel.riabchuk.patterns.factory;

public abstract class Animal {

    public abstract String makeSound();
}
