package org.hillel.riabchuk.patterns.factory;

public class Snake extends Animal {

    @Override
    public String makeSound() {
        return "Hiss!";
    }
}
