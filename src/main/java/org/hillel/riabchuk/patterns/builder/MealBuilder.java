package org.hillel.riabchuk.patterns.builder;

public interface MealBuilder {

    void buildDrink();
    void buildMainCourse();
    void buildSide();
    Meal getMeal();
}
