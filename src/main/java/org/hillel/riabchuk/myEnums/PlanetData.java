package org.hillel.riabchuk.myEnums;

public class PlanetData {

    public int PlanetOrder;
    public double SunDistance;

    public PlanetData(int planetOrder, double sunDistance) {
        PlanetOrder = planetOrder;
        SunDistance = sunDistance;
    }
}