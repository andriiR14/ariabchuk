package org.hillel.riabchuk.myEnums;

public enum Planet {
    MERCURY(1, 0.39)
            {
                @Override
                public String GetPlanetDescription() {
                    return "First planet is " + this.CombineDescription();
                }
            },
    VENUS(2, 0.723)
            {
                @Override
                public String GetPlanetDescription() {
                    return "Second planet is " + this.CombineDescription();
                }
            },
    EARTH(3, 1)
            {
                @Override
                public String GetPlanetDescription() {
                    return "Third planet is " + this.CombineDescription();
                }
            },
    MARS(4, 1.524)
            {
                @Override
                public String GetPlanetDescription() {
                    return "Fourth planet is " + this.CombineDescription();
                }
            },
    JUPITER(5, 5.203)
            {
                @Override
                public String GetPlanetDescription() {
                    return "Fifth planet is " + this.CombineDescription();
                }
            },
    SATURN(6, 9.539)
            {
                @Override
                public String GetPlanetDescription() {
                    return "Sixth planet is " + this.CombineDescription();
                }
            },
    URANUS(7, 19.18)
            {
                @Override
                public String GetPlanetDescription() {
                    return "Seventh planet is " + this.CombineDescription();
                }
            },
    NEPTUNE(8, 30.06)
            {
                @Override
                public String GetPlanetDescription() {
                    return "Eighth planet is " + this.CombineDescription();
                }
            },
    PLUTO(9, 39.53)
            {
                @Override
                public String GetPlanetDescription() {
                    return "Ninth planet is " + this.CombineDescription();
                }
            };

    public String CombineDescription() {
        return Character.toUpperCase(this.name().charAt(0)) + this.name().substring(1).toLowerCase()
                + ", planet order " + this.planetData.PlanetOrder + ", distance from the Sun is "
                + this.planetData.SunDistance + " astronomical units";
    }

    private PlanetData planetData;

    Planet(int planetOrder, double sunDistance) {

        this.planetData = new PlanetData(planetOrder, sunDistance);
    }

    abstract String GetPlanetDescription();
}