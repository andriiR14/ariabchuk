package org.hillel.riabchuk.jbdc;

import java.sql.*;
import java.text.ParseException;

public class DataBaseExample {

    private static final String BD_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/school";
    private static final String USER = "root2";
    private static final String PASSWORD = "admin";

    public static void main(String[] args) {

        try {
            getDBConnection();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static Connection getDBConnection() throws ParseException {

        try {
            Class.forName(BD_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {

            createDbUserTable(connection);
            return connection;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static void createDbUserTable(Connection connection) throws ParseException {

        String createTableSQL = "CREATE TABLE DBUSER(USER_ID INT(5) NOT NULL," +
                "USERNAME VARCHAR(20) NOT NULL" +
                "CREATED_BY VARCHAR(20) NOT NULL" +
                "PRIMARY KEY (USER_ID)" +
                ")";

        try (Statement statement = connection.createStatement()) {

            statement.execute(createTableSQL);
//            insertDataIntoDB(statement);
//            updateDataInTable(statement);
//            selectFromTable(statement);
//            deleteFromTable(statement);

            System.out.println("Table \"dbuser\" is created");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void insertDataIntoDB(Statement statement) throws ParseException {

        String insertTableSQL = "INSERT INTO DBUSER" +
                "(USER_ID, USERNAME, CREATED_BY) VALUES" +
                "(1, 'andrii', 'system')";

        try {
            statement.executeUpdate(insertTableSQL);
            System.out.println("Record is inserted to DBUSER table!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateDataInTable(Statement statement) throws ParseException {

        String updateTableSQL = "UPDATE DBUSER SET USERNAME = 'andrii_new' WHERE USER_ID = 1";

        try {
            statement.execute(updateTableSQL);
            System.out.println("Record is updated to DBUSER table!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void  selectFromTable(Statement statement) throws ParseException {

        String selectTableSQL = "SELECT USER_ID, USERNAME from DBUSER";

        try {
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {

                int userid = rs.getInt("USER_ID");
                String username = rs.getString("USERNAME");

                System.out.println("userid: " + userid);
                System.out.println("username: " + username);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void deleteFromTable(Statement statement) throws ParseException {
        String deleteFromTableSQL = "DELETE FROM DBUSER WHERE USER_ID = 1";

        try {
            statement.execute(deleteFromTableSQL);
            System.out.println("Record is deleted from DBUSER table!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
