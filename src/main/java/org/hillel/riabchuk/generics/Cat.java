package org.hillel.riabchuk.generics;

public class Cat extends Animal {

    @Override
    Cat name(Object o) {
        return this;
    }

    @Override
    Object getData() {
        return new Cat();
    }
}