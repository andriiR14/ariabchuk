package org.hillel.riabchuk.generics;

public class Dog extends Animal {

    private String dogName;

    @Override
    public Object getData() {
        return dogName;
    }

    @Override
    public Dog name(Object o) {
        this.dogName = (String)o;
        return this;
    }
}