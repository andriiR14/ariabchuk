package org.hillel.riabchuk.generics;

import java.util.Collection;

public class GenericSmpl<T> {

    void test(T t) {
        System.out.println(t.toString());
    }

    public T addAndReturn(T element, Collection<T> collection) {

        collection.add(element);
        return element;
    }
}