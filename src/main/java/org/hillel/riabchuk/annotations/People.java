package org.hillel.riabchuk.annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class People implements Serializable, Cloneable {

    private String name;
    private int age;
    public int sum;
    protected int test;
    public final String smth = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private void test() {

    }

    void test2() {

    }

    @Autowired
    @Deprecated
    public static void method(String[] params) {

    }

    @Autowired
    @Override
    public String toString() {
        return super.toString();
    }
}
