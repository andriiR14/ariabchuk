package org.hillel.riabchuk.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MyNewComponent {

    String value();

    String name();
    String age();
    String[] newNames();
    String element() default "elem";

}
