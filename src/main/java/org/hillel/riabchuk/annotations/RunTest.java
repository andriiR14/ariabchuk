package org.hillel.riabchuk.annotations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class RunTest {

    public static void main(String[] args) {

        Logger logger = LoggerFactory.getLogger(RunTest.class);

        System.out.println("Testing...");

        logger.debug("hahaha");
        int passed = 0, failed = 0, count = 0, ignore = 0;

        Class<AnnotationTest> obj = AnnotationTest.class;

        if (obj.isAnnotationPresent(TestInfo.class)) {

            Annotation annotation = obj.getAnnotation(TestInfo.class);
            TestInfo testInfo = (TestInfo) annotation;

            System.out.printf("%nPriotity :%s", testInfo.priority());
            System.out.printf("%nCreatedBy :%s", testInfo.createdBy());
            System.out.printf("%nTags :");

            int tagLenght = testInfo.tags().length;

            for (String tag : testInfo.tags()) {
                if (tagLenght > 1) {
                    System.out.println(tag + ", ");
                }
                else {
                    System.out.println(tag);
                }
                tagLenght--;
            }

            System.out.printf("%nLastModified :%s%n%n", testInfo.lastModified());

        }

        for (Method method : obj.getDeclaredMethods()) {

            if (method.isAnnotationPresent(Test.class)) {

                Annotation annotation = method.getAnnotation(Test.class);
                Test test = (Test)annotation;

                if (test.enabled()) {

                    try {
                        method.invoke(obj.newInstance());
                        System.out.printf("%s - GenericSmpl '%s' - passed %n", ++count, method.getName());
                        passed++;
                    }
                    catch (Exception ex) {
                        System.out.printf("%s - GenericSmpl '%s' - failed: %s %n", ++count, method.getName(), ex.getCause());
                        failed++;
                    }
                }
                else {
                    System.out.printf("%s - GenericSmpl '%s' - ignored %n", ++count, method.getName());
                }
            }
        }
        System.out.printf("%nResult: Total: %d, Passed: %d, Failed: %d, Ignored: %d%n", count, passed, failed, ignore);
    }
}
