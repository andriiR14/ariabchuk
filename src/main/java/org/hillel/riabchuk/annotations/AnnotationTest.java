package org.hillel.riabchuk.annotations;


@TestInfo(
        priority = TestInfo.Priority.HIGH,
        createdBy = "Andrii",
        tags = {"sales", "test"}
)
public class AnnotationTest {

    @Test
    void testA() {
        if (true)
            throw new RuntimeException("This test always failed");
    }

    @Test(enabled = false)
    void testB() {
        if (false)
            throw new RuntimeException("This test always passed");
    }

    @Test(enabled = true)
    void testC() {
        if (10 > 1) {

        }
    }
}
