package org.hillel.riabchuk.collections;

public class Empl {

    private String name;
    private int salary;

    public Empl() {
    }

    public Empl(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Empl{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }
}
