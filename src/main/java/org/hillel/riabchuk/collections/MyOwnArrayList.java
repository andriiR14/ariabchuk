package org.hillel.riabchuk.collections;

import java.io.Serializable;
import java.util.RandomAccess;

public class MyOwnArrayList<T> implements RandomAccess, Cloneable, Serializable {

    private transient T[] elementData;
    private int size;
    protected transient int modCount = 0;
    private static final long seriesVersionUID = 1234L;

    public MyOwnArrayList() {
        this(10);
    }

    public MyOwnArrayList(int initCapacity) {

        super();
        if (initCapacity < 0) {
            throw new NegativeArraySizeException("Negative Array Capacity: " + initCapacity);
        }

        T t[] = (T[]) new Object[initCapacity];
        this.elementData = t;
    }

    public boolean add(T obj) {
        validateCapacity(size + 1);
        elementData[size++] = obj;
        return true;
    }

    public T get(int index) {
        return elementData[index];
    }

    public int size() {
        return size;
    }

    public void validateCapacity(int minCapacity) {
        modCount++;
        int oldCapacity = elementData.length;
        if (minCapacity > oldCapacity) {
            Object oldData[] = elementData;
            int newCapacity = oldCapacity*3/2;

            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }

            T t[] = (T[]) new Object[newCapacity];
            elementData = t;
            System.arraycopy(oldData, 0, elementData, 0, size);
        }
    }

    public T remove(int index) {

        if (index >= size || index < 0)
            throw new IndexOutOfBoundsException("Illegal index: " + index);

        modCount++;
        T oldValue = elementData[index];

        int numMoved = size - index - 1;
        if (numMoved > 0)
            System.arraycopy(elementData, index + 1, elementData, index, numMoved);

        elementData[--size] = null;
        return oldValue;
    }

    public FruitIterator iterator() {
        System.out.println("My overrided iterator method called in Fruit class");
        return new FruitIterator(this);
    }

}
