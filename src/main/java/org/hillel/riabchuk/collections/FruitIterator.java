package org.hillel.riabchuk.collections;

public class FruitIterator {

    private MyOwnArrayList fruitList;
    private int position;

    public FruitIterator(MyOwnArrayList fruitList) {
        this.fruitList = fruitList;
    }

    public MyOwnArrayList getFruitList() {
        return fruitList;
    }

    public void setFruitList(MyOwnArrayList fruitList) {
        this.fruitList = fruitList;
    }

    public boolean hasNext() {
        return position < fruitList.size();
    }

    public Object next() {
        return fruitList.get(position++);
    }

    public void remove() {
        fruitList.remove(position);
    }
}
